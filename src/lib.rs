use std::error::Error;

#[macro_use]
extern crate pest_derive;

mod arithmetic_expression_parser;

mod arithmetic_expressions {
    use std::{error::Error, fmt};

    #[derive(Debug, PartialEq, Clone, Default)]
    pub struct Info {
        pub start: usize,
        pub end: usize,
    }

    #[derive(Debug, PartialEq, Clone)]
    pub enum Term {
        True(Info),
        False(Info),
        If(Info, Box<Term>, Box<Term>, Box<Term>),
        Zero(Info),
        Succ(Info, Box<Term>),
        Pred(Info, Box<Term>),
        IsZero(Info, Box<Term>),
    }

    impl fmt::Display for Term {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            match self {
                Term::True(_) => write!(f, "true"),
                Term::False(_) => write!(f, "false"),
                Term::If(_, t1, t2, t3) => write!(f, "if {} then {} else {}", t1, t2, t3),
                Term::Zero(_) => write!(f, "0"),
                Term::Succ(_, t1) => write!(f, "succ {}", t1),
                Term::Pred(_, t1) => write!(f, "pred {}", t1),
                Term::IsZero(_, t1) => write!(f, "iszero {}", t1),
            }
        }
    }
    fn is_numeric_value(term: &Term) -> bool {
        match term {
            Term::Zero(_) => true,
            Term::Succ(_, term_1) => is_numeric_value(term_1),
            _ => false,
        }
    }

    fn is_value(term: &Term) -> bool {
        match term {
            Term::True(_) => true,
            Term::False(_) => true,
            term if is_numeric_value(term) => true,
            _ => false,
        }
    }

    pub fn evaluate_internal(term: Term) -> Result<Term, Box<dyn Error>> {
        match term {
            Term::If(info, term_1, term_2, term_3) => match *term_1 {
                Term::True(_) => Ok(*term_2),
                Term::False(_) => Ok(*term_3),
                term_1 => {
                    let term_1_evaluated = evaluate_internal(term_1)?;
                    Ok(Term::If(info, Box::new(term_1_evaluated), term_2, term_3))
                }
            },
            Term::Succ(info, term_1) => {
                let term_1_evaluated = evaluate_internal(*term_1)?;
                Ok(Term::Succ(info, Box::new(term_1_evaluated)))
            }
            Term::Pred(info, term_1) => match *term_1 {
                Term::Zero(_) => Ok(Term::Zero(Info::default())),
                Term::Succ(_, value_1) if (is_numeric_value(&*value_1)) => Ok(*value_1),
                term_1 => {
                    let term_1_evaluated = evaluate_internal(term_1)?;

                    Ok(Term::Pred(info, Box::new(term_1_evaluated)))
                }
            },
            Term::IsZero(info, term_1) => match *term_1 {
                Term::Zero(_) => Ok(Term::True(Info::default())),
                Term::Succ(_, value_1) if (is_numeric_value(&*value_1)) => {
                    Ok(Term::False(Info::default()))
                }
                term_1 => {
                    let term_1_evaluated = evaluate_internal(term_1)?;

                    Ok(Term::IsZero(info, Box::new(term_1_evaluated)))
                }
            },
            other_term => {
                let err: Box<dyn Error + Send + Sync> =
                    From::from(format!("No evaluation rule applies {:?}", other_term));
                Err(err as Box<dyn Error>)
            }
        }
    }

    pub fn evaluate(term: Term) -> Term {
        if let Ok(term_1) = evaluate_internal(term.clone()) {
            evaluate(term_1)
        } else {
            term
        }
    }

    fn evaluate_inner(term: Term) -> Term {
        match term {
            Term::If(info, term_1, term_2, term_3) => match *term_1 {
                Term::True(_) => *term_2,
                Term::False(_) => *term_3,
                term_1 => {
                    let term_1_evaluated = evaluate_inner(term_1);
                    Term::If(info, Box::new(term_1_evaluated), term_2, term_3)
                }
            },
            Term::Succ(info, term_1) => {
                let term_1_evaluated = evaluate_inner(*term_1);
                Term::Succ(info, Box::new(term_1_evaluated))
            }
            Term::Pred(info, term_1) => match *term_1 {
                Term::Zero(_) => Term::Zero(Info::default()),
                Term::Succ(_, value_1) if (is_numeric_value(&*value_1)) => *value_1,
                term_1 => {
                    let term_1_evaluated = evaluate_inner(term_1);

                    Term::Pred(info, Box::new(term_1_evaluated))
                }
            },
            Term::IsZero(info, term_1) => match *term_1 {
                Term::Zero(_) => Term::True(Info::default()),
                Term::Succ(_, value_1) if (is_numeric_value(&*value_1)) => {
                    Term::False(Info::default())
                }
                term_1 => {
                    let term_1_evaluated = evaluate_inner(term_1);

                    Term::IsZero(info, Box::new(term_1_evaluated))
                }
            },
            other_term => other_term,
        }
    }

    pub fn _evaluate(term: Term) -> Term {
        let evaluated_inner = evaluate_inner(term);
        evaluate_inner(evaluated_inner)
    }
}

pub fn interpret(input: &str) -> Result<String, Box<dyn Error>> {
    let ast = arithmetic_expression_parser::parse(input)?;
    let result = arithmetic_expressions::evaluate(ast);

    Ok(format!("{}", result))
}

#[cfg(test)]
mod tests {
    use test_case::test_case;

    use super::arithmetic_expressions::*;
    use super::*;

    #[test_case( Term::Succ(Info::default(), Box::new(Term::Pred(Info::default(),Box::new(Term::Zero(Info::default()))))), Term::Succ(Info::default(), Box::new(Term::Zero(Info::default())))  ; "E-Succ")]
    #[test_case( Term::Pred(Info::default(),Box::new(Term::Zero(Info::default()))), Term::Zero(Info::default())  ; "E-PredZero")]
    #[test_case( Term::Pred(Info::default(),Box::new(Term::Succ(Info::default(), Box::new(Term::Zero(Info::default()))))), Term::Zero(Info::default())  ; "E-PredSucc")]
    #[test_case( Term::Pred(Info::default(), Box::new(Term::Succ(Info::default(),Box::new(Term::Pred(Info::default(),Box::new(Term::Zero(Info::default()))))))), Term::Zero(Info::default())  ; "E-Pred-Total")]
    #[test_case( Term::IsZero(Info::default(),Box::new(Term::Zero(Info::default()))), Term::True(Info::default())  ; "E-IsZeroZero")]
    #[test_case( Term::IsZero(Info::default(),Box::new(Term::Succ(Info::default(), Box::new(Term::Zero(Info::default()))))), Term::False(Info::default())  ; "E-IsZeroSucc")]
    #[test_case( Term::IsZero(Info::default(), Box::new(Term::Pred(Info::default(), Box::new(Term::Succ(Info::default(), Box::new(Term::Zero(Info::default()))))))), Term::True(Info::default())  ; "E-IsZero-Total")]
    #[test_case( Term::Pred(Info::default(),Box::new(Term::Succ(Info::default(), Box::new(Term::True(Info::default()))))), Term::Pred(Info::default(),Box::new(Term::Succ(Info::default(), Box::new(Term::True(Info::default())))))  ; "E-PredSucc-Stuck")]
    fn test_evaluate(term: Term, expected: Term) {
        let result = evaluate(term);

        assert_eq!(result, expected)
    }

    #[test_case( "if iszero 0 then if true then true else false else false", "true"  ; "evaluate totally")]
    fn test_interpret(input: &str, expected: &str) {
        let result = interpret(input).unwrap();

        assert_eq!(result, expected)
    }
}
