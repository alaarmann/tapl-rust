use std::error::Error;

use crate::arithmetic_expressions::{Info, Term};
use pest::Parser;
use pest::{iterators::Pair, Span};

#[derive(Parser)]
#[grammar = "arithmetic_expression.pest"]
pub struct ArithmeticExpressionParser;
impl From<Span<'_>> for Info {
    fn from(span: Span) -> Self {
        Info {
            start: span.start(),
            end: span.end(),
        }
    }
}

impl From<Pair<'_, Rule>> for Term {
    fn from(pair: Pair<Rule>) -> Self {
        match pair.as_rule() {
            Rule::constant_true => Term::True(Info::from(pair.as_span())),
            Rule::constant_false => Term::False(Info::from(pair.as_span())),
            Rule::conditional => {
                let span = pair.as_span();
                let mut inner = pair.into_inner();
                Term::If(
                    Info::from(span),
                    Box::new(Term::from(inner.next().unwrap())),
                    Box::new(Term::from(inner.next().unwrap())),
                    Box::new(Term::from(inner.next().unwrap())),
                )
            }
            Rule::constant_zero => Term::Zero(Info::from(pair.as_span())),
            Rule::successor => Term::Succ(
                Info::from(pair.as_span()),
                Box::new(Term::from(pair.into_inner().next().unwrap())),
            ),
            Rule::predecessor => Term::Pred(
                Info::from(pair.as_span()),
                Box::new(Term::from(pair.into_inner().next().unwrap())),
            ),
            Rule::zero_test => Term::IsZero(
                Info::from(pair.as_span()),
                Box::new(Term::from(pair.into_inner().next().unwrap())),
            ),
            _ => unreachable!(),
        }
    }
}

pub fn parse(input: &str) -> Result<Term, Box<dyn Error>> {
    let parsed = ArithmeticExpressionParser::parse(Rule::expression, input)?
        .next()
        .unwrap()
        .into_inner()
        .next()
        .unwrap();

    let message_definition = Term::from(parsed);
    Ok(message_definition)
}

#[cfg(test)]
mod tests {
    use test_case::test_case;

    use super::*;

    #[test_case( "0", Term::Zero(Info{start: 0, end: 1}) ; "constant_zero")]
    #[test_case( "succ 0", Term::Succ(Info { start: 0, end: 6 }, Box::new(Term::Zero(Info { start: 5, end: 6 }))) ; "successor")]
    #[test_case( "if true then 0 else succ 0", Term::If(Info{start: 0, end: 26}, Box::new(Term::True(Info{start: 3, end: 7})), Box::new(Term::Zero(Info{start:13, end:14})), Box::new(Term::Succ(Info{start: 20, end: 26}, Box::new(Term::Zero(Info{start:25, end:26}))))) ; "conditional")]
    fn parse_expression(input: &str, expected: Term) {
        let result = parse(input).unwrap();

        assert_eq!(result, expected)
    }
}
