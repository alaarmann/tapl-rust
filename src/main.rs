use std::{
    io::{self, Read},
    process,
};

fn main() {
    let mut buffer = String::new();
    if let Err(e) = io::stdin().read_to_string(&mut buffer) {
        eprintln!("Error reading input: {}", e);

        process::exit(1);
    }

    let result = tapl_rust::interpret(buffer.as_str());

    if let Err(e) = result {
        eprintln!("Interpreter error: {}", e);

        process::exit(1);
    }

    println!("{}\n", result.unwrap());
}
